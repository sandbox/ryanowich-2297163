<?php

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *	 An array containing the breadcrumb links.
 * @return
 *	 A string containing the breadcrumb output.
 */
function gladfagskole_breadcrumb($variables){
	$breadcrumb = $variables['breadcrumb'];
	if (!empty($breadcrumb)) {
		$breadcrumb[] = drupal_get_title();
		return '<div class="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>';
	}
}

function gladfagskole_preprocess_html(&$variables) {
	// Add variables for path to theme.
	$variables['base_path'] = base_path();
	$variables['path_to_theme'] = drupal_get_path('theme', 'gladfagskole');
	
	// Add body classes if certain regions have content.
	if (!empty($variables['page']['featured'])) {
		$variables['classes_array'][] = 'featured';
	}
	
	// Load FontAwesome
	drupal_add_css('//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', array(
		'type' => 'external'
	));
}

/**
* Implementation of hook_preprocess_page().
*/

function gladfagskole_preprocess_page(&$variables) {
	
	
	# From http://www.lullabot.com/blog/article/customizable-header-images-your-drupal-theme
	$settings = theme_get_setting('gladfagskole');
	if (!empty($settings['header_image_path'])) {
		$variables['header_image_path'] = $settings['header_image_path'];
	}
	else {
		$variables['header_image_path'] = path_to_theme().'/img/banner_default.png';
	}
	
	
	
	# From http://othermachines.com/blog/drupal-override-output-taxonomy-term-page-vocabulary
	if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
		$term = taxonomy_term_load(arg(2));
		//$variables['mytemplate'] = $term->vocabulary_machine_name;
		$variables['theme_hook_suggestions'][] = 'page--taxonomy--term--' . $term->vocabulary_machine_name . '.tpl.php';
		$variables['theme_hook_suggestions'][] = 'page--taxonomy--term--' . $term->vocabulary_machine_name;
	}
	
	
	/*
	# check if current page is taxonomy term page
	if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2)) && !arg(3)) {
		echo 'This a taxonomy page!';
		
		//$term = taxonomy_get_term($taxonomy_id);
		//$term = taxonomy_term_load($taxonomy_id);
		//$variables['term_name'] = $terms->name;		 // Store anything you want to use later in $variables
		
		# term load
		$term = taxonomy_term_load(arg(2));
		$variables['term'] = $term;
	}
	*/
	
	/*
	# From http://stefanpetrov.net/en/blog/drupal-7-theming-taxonomy-term-page
	# declarate list items variable
	$items = array();
 
	# check if current page is taxonomy term page
	if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2)) && !arg(3)) {
	
		# term load
		$term = taxonomy_term_load(arg(2));
	
		# copy content so for later use
		$nodes = $variables['page']['content']['system_main']['nodes'];
		
		//dpr($nodes);
		//$variables['term'] = $term;
		
		# generate list items
		foreach ($nodes as $key => $node) {
			if (is_numeric($key)) $items[]['data'] = render($node);
		}
		
		# we declare that this content should be themed like list items
		$variables['page']['content']['system_main']['nodes'] = array(
			'#theme' => 'item_list',
			'#items' => $items,
			'#attributes' => array('id' => 'THE_UL_ID', 'class' => array('THE_UL_FIRST_CLASS', 'THE_UL_SECOND_CLASS')),
		);
	}
	*/
	
}

/**
 * Override or insert variables into the page template.
 */
function gladfagskole_process_page(&$variables) {
	// Always print the site name and slogan, but if they are toggled off, we'll
	// just hide them visually.
	$variables['hide_site_name']	 = theme_get_setting('toggle_name') ? FALSE : TRUE;
	$variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
	if ($variables['hide_site_name']) {
		// If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
		$variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
	}
	if ($variables['hide_site_slogan']) {
		// If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
		$variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
	}
	// Since the title and the shortcut link are both block level elements,
	// positioning them next to each other is much simpler with a wrapper div.
	if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
		// Add a wrapper div using the title_prefix and title_suffix render elements.
		$variables['title_prefix']['shortcut_wrapper'] = array(
			'#markup' => '<div class="shortcut-wrapper clearfix">',
			'#weight' => 100,
		);
		$variables['title_suffix']['shortcut_wrapper'] = array(
			'#markup' => '</div>',
			'#weight' => -99,
		);
		// Make sure the shortcut link is the first item in title_suffix.
		$variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
	}
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function gladfagskole_preprocess_maintenance_page(&$variables) {
	// By default, site_name is set to Drupal if no db connection is available
	// or during site installation. Setting site_name to an empty string makes
	// the site and update pages look cleaner.
	// @see template_preprocess_maintenance_page
	if (!$variables['db_is_active']) {
		$variables['site_name'] = '';
	}
	drupal_add_css(drupal_get_path('theme', 'gladfagskole') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function gladfagskole_process_maintenance_page(&$variables) {
	// Always print the site name and slogan, but if they are toggled off, we'll
	// just hide them visually.
	$variables['hide_site_name']	 = theme_get_setting('toggle_name') ? FALSE : TRUE;
	$variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
	if ($variables['hide_site_name']) {
		// If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
		$variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
	}
	if ($variables['hide_site_slogan']) {
		// If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
		$variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
	}
}

/**
 * Override or insert variables into the node template.
 */
function gladfagskole_preprocess_node(&$variables) {
	if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
		$variables['classes_array'][] = 'node-full';
	}
	
	# Post date for Aktuelt
	$node = $variables['node'];
	// field_get_items() always returns an array, even if the field is limited to one value
	$field_items = field_get_items('node', $node, 'field_nyhed');
	// Get the value from the first item in the array (if that's the one you need)
	//$is_aktuelt = $field_items[0]['value'];
	
	if ($field_items[0]['value'] == 1) {
		//$variables['submitted'] = $variables['date'] . ' — ' . $variables['name'];
		//$variables['aktuelt_date'] = $aktuelt_date;
		//$variables['date']= format_date($variables['created'] ,'your date format');
		//$variables['aktuelt_date'] = format_date($variables['elements']['#node']->created', 'custom', l, F d, Y', );
		$aktuelt_date = $variables['elements']['#node']->created;
		$aktueltDateCode = '';
		$aktueltDateCode .= '<div class="post_date_gfx">';
			$aktueltDateCode .= '<span class="day">' . date('d', $aktuelt_date) . '</span>';
				$aktueltDateCode .= '<span class="month_year">';
					$aktueltDateCode .= '<span class="month">' . date('M', $aktuelt_date) . '</span>';
					$aktueltDateCode .= '<span class="year">' . date('Y', $aktuelt_date) . '</span>';
				$aktueltDateCode .= '</span>';
		$aktueltDateCode .= '</div>';
		
		# Set date-variable for printing
		$variables['aktuelt_date_formatted'] = $aktueltDateCode;
		# Add body class
		$variables['classes_array'][] = 'is_aktuelt';
	}
}

/**
 * Override or insert variables into the block template.
 */
function gladfagskole_preprocess_block(&$variables) {
	// In the header region visually hide block titles.
	if ($variables['block']->region == 'header') {
		$variables['title_attributes_array']['class'][] = 'element-invisible';
	}
	
	/*
	# From http://atendesigngroup.com/blog/adding-css-classes-blocks-drupal
	// Set shortcut variables. Hooray for less typing!
	$block_id = $vars['block']->module . '-' . $vars['block']->delta;
	$classes = &$vars['classes_array'];
	$title_classes = &$vars['title_attributes_array']['class'];
	$content_classes = &$vars['content_attributes_array']['class'];

	// Add global classes to all blocks
	$title_classes[] = 'block-title';
	$content_classes[] = 'block-content';

	// Uncomment the line below to see variables you can use to target a field
	print $block_id . '<br/>';

	// Add classes based on the block delta.
	switch ($block_id) {
		//System Navigation block
		case 'system-navigation':
			$classes[] = 'block-rounded';
			$title_classes[] = 'block-fancy-title';
			$content_classes[] = 'block-fancy-content';
			break;
		//Main Menu block
		case 'system-main-menu':
		//User Login block
		case 'user-login':
			$title_classes[] = 'element-invisible';
			break;
	}
	*/	
}

/**
 * Implements theme_menu_tree().
 */
function gladfagskole_menu_tree($variables) {
	return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function gladfagskole_field__taxonomy_term_reference($variables) {
	$output = '';

	// Render the label, if it's not hidden.
	if (!$variables['label_hidden']) {
		$output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
	}

	// Render the items.
	$output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
	foreach ($variables['items'] as $delta => $item) {
		$output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
	}
	$output .= '</ul>';

	// Render the top-level DIV.
	$output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

	return $output;
}

/**
 * Add javascript files for page--front jquery slideshow.
 */
if (drupal_is_front_page()) {
	drupal_add_js(drupal_get_path('theme', 'gladfagskole') . '/js/jquery.cycle2.min.js');
	drupal_add_js(drupal_get_path('theme', 'gladfagskole') . '/js/flowtype.min.js');
	
	// Jquery options here:
	// http://jquery.malsup.com/cycle/options.html

	//Initialize slideshow using theme settings
	$effect=theme_get_setting('slideshow_effect');
	$slide_time=theme_get_setting('slideshow_slide_time')*1000;
	drupal_add_js("jQuery(document).ready(function($) {
		
		// Prevent h2.title display bug
		/******************************/
		//$('.slideshow').on('cycle-bootstrap', function( event, opts ) {
				$('.slide_image h2.slide_title').addClass('hidden');
			//console.log('h2.title hidden');
		//});
		
		$('.slideshow').on('cycle-initialized', function( event, opts ) {
			//console.log('cycle-initialized');
				$('.slide_image h2.slide_title').removeClass('hidden');
			//console.log('h2.title visible');
			
			$('.slide_image h2.slide_title').addClass('animated fadeInLeft');
	
			$('.slideshow').flowtype({
				minimum	 : 320,
				maximum	 : 941,
				minFont	 : 5,
				maxFont	 : 14,
				fontRatio : 40,
				lineRatio : 1.0
			});
		});
		/******************************/
				
		if (!$('.slideshow_pager').length) {
			$('.slideshow').after('<ul class=\"slideshow_pager\"></ul>');
		}
				
		$('.slideshow').cycle({
			loader: true,
			//slides: '> div.slide',
			slides: ' div.slide',
			pager: '.slideshow_pager',
			pagerTemplate: '<li><a href=\"#\">{{slideNum}}</a></li>',
			pagerActiveClass: 'activeSlide',
			autoHeight: 'calc',
			//autoHeight: 'false',
			swipe: true,
			fx: '".$effect."',
			timeout: ".$slide_time.",
			//timeout: 500000,
			speed: 500,
			manualSpeed: 100,
			log: true
		});
		
		// Position: http://www.dummies.com/how-to/content/inserting-content-before-after-and-inside-elements.html
		$('ul.slideshow_pager').before('<div class=\"slideshow_progress\"></div>');
		
		// Remove dulicate pager
		//$('.slideshow ul.slideshow_pager').not(':last')​.remove();
		
		
		
	});",
	array('type' => 'inline', 'scope' => 'header', 'weight' => 5)
	);
}

/*
# From http://www.coolestguidesontheplanet.com/downtown/theme-specific-drupal-taxonomy-tag-templates
function gladfagskole_preprocess_taxonomy_term(&$variables) {
	$variables['view_mode'] = $variables['elements']['#view_mode'];
	$variables['term'] = $variables['elements']['#term'];
	$term = $variables['term'];

	$uri = entity_uri('taxonomy_term', $term);
	$variables['term_url']	= url($uri['path'], $uri['options']);
	$variables['term_name'] = check_plain($term-name);
	$variables['page']			= $variables['view_mode'] == 'full' && taxonomy_term_is_page($term);

	// Flatten the term object's member fields.
	$variables = array_merge((array) $term, $variables);

	// Helpful $content variable for templates.
	$variables['content'] = array();
	foreach (element_children($variables['elements']) as $key) {
		$variables['content'][$key] = $variables['elements'][$key];
	}

	// field_attach_preprocess() overwrites the $[field_name] variables with the
	// values of the field in the language that was selected for display, instead
	// of the raw values in $term->[field_name], which contain all values in all
	// languages.
	field_attach_preprocess('taxonomy_term', $term, $variables['content'], $variables);

	// Gather classes, and clean up name so there are no underscores.
	$vocabulary_name_css = str_replace('_', '-', $term->vocabulary_machine_name);
	$variables['classes_array'][] = 'vocabulary-' . $vocabulary_name_css;

	$variables['theme_hook_suggestions'][] = 'taxonomy_term__' . $term->vocabulary_machine_name;
	$variables['theme_hook_suggestions'][] = 'taxonomy_term__' . $term->tid;
	//$variables['theme_hook_suggestions'][] = 'taxonomy_term__' . $term->tvocabulary_machine_name;
}
*/

# From http://www.elvisblogs.org/drupal/drupal-list-child-taxonomy-terms-current-parent-term#sthash.GqindWJD.dpuf
function gladfagskole_taxChildTerms($vid) {
	if(arg(0) == 'taxonomy' && arg(1) == 'term') {
		// Check term name
		$term = taxonomy_term_load(arg(2));
		//$variables['mytemplate'] = $term->vocabulary_machine_name;
		
		//if($term->vocabulary_machine_name == 'faglinjer' || $term->vocabulary_machine_name == 'uddannelser') {
		if($term->vocabulary_machine_name != 'tags') {
			$children = taxonomy_get_children(arg(2), $vid);
				if(!$children) {
					$custom_parent = taxonomy_get_parents(arg(2));
						$parent_tree = array();
						foreach ($custom_parent as $custom_child => $key) {
							$parent_tree = taxonomy_get_tree($vid, $key->tid);
						}
						$children = $parent_tree;
				}
	 
			//$output = '<ul class="faglinjer_childterms">';
			// Modified for both faglinjer and uddannelser
			$output = '<ul class="'.$term->vocabulary_machine_name.'_childterms">';
			
			//var_dump(count($children));
			$taxChildenCount = count($children);
			$variables['taxChildenCount'] = $taxChildenCount;
			if($taxChildenCount > 0) {
				$taxHasChilden = true;
			} else {
				$taxHasChilden = false;
			}
			$variables['taxHasChilden'] = $taxHasChilden;
			
			
			foreach ($children as $term) {
				$output .= '<li>';
				$output .= l($term->name, 'taxonomy/term/' . $term->tid);
				$output .= '</li>';
			}
			$output .= '</ul>';
			
			if($taxHasChilden) {
				return $output;
			}
		}
	}
}

/*
# From http://joetower.com/2011/11/drupal-7-theming-views-slideshow
function gladfagskole_preprocess_views_view_fields(&$variables, $hook){
	
	//dpm($variables['view']);
	//echo 'View name: ' . $variables['view']->name;
	
	if ($variables['view']->name == 'slideshow_front_v3') {
		echo 'View name: ' . $variables['view']->name;
		//dpm($variables['view']);
		//dpm($variables['fields']);
		dpm($variables);
		//$variables['fields']['title']->wrapper_prefix = '<div class="slideshow-content-wrapper">' . 
		//$variables['fields']['title']->wrapper_prefix;
		//$variables['fields']['Closing_Field_Name']->wrapper_suffix .= '</div>';
		//dpm($variables['fields']['field_image_1']);
	}
}
*/

/*
function gladfagskole_preprocess_views_view_fields(&$vars) {
	$view = $vars['view'];
	//echo 'View name: ' . $vars['view']->name;
	
	if ($view->name == 'slideshow_front_v3') {
		//dpm($view->name);
	}

	// Loop through the fields for this view.
	$previous_inline = FALSE;
	$vars['fields'] = array(); // ensure it's at least an empty array.
	foreach ($view->field as $id => $field) {
		// render this even if set to exclude so it can be used elsewhere.
		$field_output = $view->style_plugin->get_field($view->row_index, $id);
		$empty = $field->is_value_empty($field_output, $field->options['empty_zero']);
		if (empty($field->options['exclude']) && (!$empty || (empty($field->options['hide_empty']) && empty($vars['options']['hide_empty'])))) {
			$object = new stdClass();
			$object->handler = &$view->field[$id];
			$object->inline = !empty($vars['options']['inline'][$id]);

			$object->element_type = $object->handler->element_type(TRUE, !$vars['options']['default_field_elements'], $object->inline);
			if ($object->element_type) {
				$class = '';
				if ($object->handler->options['element_default_classes']) {
					$class = 'field-content';
				}

				if ($classes = $object->handler->element_classes($view->row_index)) {
					if ($class) {
						$class .= ' ';
					}
					$class .= $classes;
				}

				$pre = '<' . $object->element_type;
				if ($class) {
					$pre .= ' class="' . $class . '"';
				}
				$field_output = $pre . '>' . $field_output . '</' . $object->element_type . '>';
			}

			// Protect ourself somewhat for backward compatibility. This will prevent
			// old templates from producing invalid HTML when no element type is selected.
			if (empty($object->element_type)) {
				$object->element_type = 'span';
			}

			$object->content = $field_output;
			if (isset($view->field[$id]->field_alias) && isset($vars['row']->{$view->field[$id]->field_alias})) {
				$object->raw = $vars['row']->{$view->field[$id]->field_alias};
			}
			else {
				$object->raw = NULL; // make sure it exists to reduce NOTICE
			}

			if (!empty($vars['options']['separator']) && $previous_inline && $object->inline && $object->content) {
				$object->separator = filter_xss_admin($vars['options']['separator']);
			}

			$object->class = drupal_clean_css_identifier($id);

			$previous_inline = $object->inline;
			$object->inline_html = $object->handler->element_wrapper_type(TRUE, TRUE);
			if ($object->inline_html === '' && $vars['options']['default_field_elements']) {
				$object->inline_html = $object->inline ? 'span' : 'div';
			}

			// Set up the wrapper HTML.
			$object->wrapper_prefix = '';
			$object->wrapper_suffix = '';

			if ($object->inline_html) {
				$class = '';
				if ($object->handler->options['element_default_classes']) {
					$class = "views-field views-field-" . $object->class;
				}

				if ($classes = $object->handler->element_wrapper_classes($view->row_index)) {
					if ($class) {
						$class .= ' ';
					}
					$class .= $classes;
				}

				$object->wrapper_prefix = '<' . $object->inline_html;
				if ($class) {
					$object->wrapper_prefix .= ' class="' . $class . '"';
				}
				$object->wrapper_prefix .= '>';
				$object->wrapper_suffix = '</' . $object->inline_html . '>';
				
				# DEV
				$object->wrapper_prefix = '';
				$object->wrapper_suffix = '';
			}

			// Set up the label for the value and the HTML to make it easier
			// on the template.
			$object->label = check_plain($view->field[$id]->label());
			$object->label_html = '';
			if ($object->label) {
				$object->label_html .= $object->label;
				if ($object->handler->options['element_label_colon']) {
					$object->label_html .= ': ';
				}

				$object->element_label_type = $object->handler->element_label_type(TRUE, !$vars['options']['default_field_elements']);
				if ($object->element_label_type) {
					$class = '';
					if ($object->handler->options['element_default_classes']) {
						$class = 'views-label views-label-' . $object->class;
					}

					$element_label_class = $object->handler->element_label_classes($view->row_index);
					if ($element_label_class) {
						if ($class) {
							$class .= ' ';
						}

						$class .= $element_label_class;
					}

					$pre = '<' . $object->element_label_type;
					if ($class) {
						$pre .= ' class="' . $class . '"';
					}
					$pre .= '>';
					
					$object->label_html = $pre . $object->label_html . '</' . $object->element_label_type . '>';
				}
			}

			$vars['fields'][$id] = $object;
		}
	}

}
*/

/*
//function gladfagskole_preprocess_views_view(&$vars) {
function gladfagskole_preprocess_views_view_field(&$vars) {
	$view = $vars['view'];
	if ($view->name == 'slideshow_front_v3') {
		dpm($view->name);
		// Add desired manipulations for all 'videos' views
		if ('videos' == $view->current_display) {
			// Add desired manipulations for the 'videos' display only
		}
	}
}
*/