/* Scripts */

jQuery(document).ready(function($) {
	
	$('.slideshow').hover(
		//console.log('hover');
		function () {
			//$(this).find('.slide_link').addClass("hover").end().find('input').focus();
			$(this).find('.slide_link').addClass("hover");
		},
		function () {
			//$(this).find('.slide_link').removeClass("hover").end().find('input').val('');
			$(this).find('.slide_link').removeClass("hover");
		}
	);
	
	// From http://haizdesign.com/blog/jquery/jquery-mouseover-mouseout-combined-function/
	$("#sidebar-right .content .latest-imagevideo").hover(function() {
		$("#sidebar-right .content .latest-imagevideo").removeClass('master neighbour');
		$(this).addClass('master');
		if ($(this).hasClass("views-row-odd")) {
			$(this).next().addClass('neighbour');
		} else if ($(this).hasClass("views-row-even")) {
			$(this).prev().addClass('neighbour');
		}
	}, function() {
		$("#sidebar-right .content .latest-imagevideo").removeClass('master neighbour');
	});
	
	// Responsive video
	$(".content").fitVids();
	
});
