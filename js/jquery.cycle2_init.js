/* Scripts */

jQuery(document).ready(function($) {
	
	$('.slideshow').after('<ul class="slideshow_pager"></ul>').cycle({
		loader: true,
		slides: '> div.slide',
		pager: '.slideshow_pager',
		pagerTemplate: '<li><a href="#">{{slideNum}}</a></li>',
		pagerActiveClass: 'activeSlide',
		autoHeight: 'calc',
		swipe: true,
		fx: 'fade',
		speed: 600,
		manualSpeed: 100,
		log: true
	});
	
	$('.slideshow').flowtype({
		minimum   : 320,
		maximum   : 941,
		minFont   : 5,
		maxFont   : 14,
		fontRatio : 40,
		lineRatio : 1.0
	});

	
});