<?php

/**
 * @file
 * Implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in page.tpl.php.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 * @see bartik_process_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
	<?php print $head; ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="MobileOptimized" content="width" />
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="cleartype" content="on" />
	<title><?php print $head_title; ?></title>
	<?php print $styles; ?>
	<?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
	<script type="text/javascript" id="gladsharedtopbarscript" src="http://www.gladfonden.dk/topbar/js/gladsharedtopbar.min.js"></script>
	<div id="skip-link">
		<a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
	</div>

	<div id="page-wrapper"><div id="page">

		<header id="header" role="banner" class="<?php print $secondary_menu ? 'with-secondary-menu': 'without-secondary-menu'; ?>"><div class="section clearfix">

			<?php if ($logo): ?>
				<div id="logo_wrapper">
					<a href="<?php print $front_page; ?>" 
						title="<?php print t('Home'); ?>"
						rel="home" id="logo">
						<img src="<?php print $logo; ?>" 
							<?php
								if ($site_name) {
									print 'alt="' . $site_name . '" />';
								} else {
									print 'alt="' . t('Home') . '" />';
								}
							?>
					</a>

					<?php if ($site_slogan): ?>
						<span id="logo_subtitle"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
								<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
									<span><?php print $site_slogan; ?></span>
								</a>
						</span>
					<?php endif; ?>

				</div>
			<?php endif; ?>

			<div id="banner">
				<img src="<?php print file_create_url(theme_get_setting('banner_path')); ?>" 
					<?php
						if ($site_name) {
							print 'alt="' . $site_name . '"';
						}
					?>
				/>
			</div>

			<?php if ($main_menu): ?>
				<nav id="main-menu" role="navigation" class="navigation">
					<?php print theme('links__system_main_menu', array(
						'links' => $main_menu,
						'attributes' => array(
							'id' => 'main-menu-links',
							'class' => array('links', 'clearfix'),
						),
						'heading' => array(
							'text' => t('Main menu'),
							'level' => 'h2',
							'class' => array('element-invisible'),
						),
					)); ?>
				</nav> <!-- /#main-menu -->
			<?php endif; ?>

		</div></header> <!-- /.section, /#header -->

		<?php if ($messages): ?>
			<div id="messages"><div class="section clearfix">
				<?php print $messages; ?>
			</div></div> <!-- /.section, /#messages -->
		<?php endif; ?>

		<div id="main-wrapper"><div id="main" class="clearfix">
			<div id="content" class="column"><div class="section">
				<a id="main-content"></a>
				<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
				<?php print $content; ?>
			</div></div> <!-- /.section, /#content -->
		</div></div> <!-- /#main, /#main-wrapper -->

	</div></div> <!-- /#page, /#page-wrapper -->

</body>
</html>
