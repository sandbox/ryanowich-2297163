Theme: Glad Fagskole (shared)

Used for www.gladfagskole.dk and subsites. 
Also base for www.gladerhverv.dk and www.gladjob.dk.

___________________________________________________________________

Installation (as any other theme):
- Download theme
- Unpack the downloaded file and place the gladfagskole folder 
  in your Drupal installation under one of the following locations:
    - sites/all/themes
    - sites/default/themes
    - sites/example.com/themes 

- Log in as an administrator on your Drupal site and go to:
  Administer > Site building > Themes (admin/build/themes)
  and make gladfagskole the default theme.

___________________________________________________________________

Theme options:
- Upload logo and banner
- Adjust slideshow speed

Features:
Two main content types:
- Article
- Gallery
- (and special content type for multiple contacts)
Automated Slideshow
- Promote node to front-page slideshow
- Custom crop to fit slideshow format
Right sidebar
- with visual latest news from all nodes
- or from specific taxonomies
- also possible to list child taxonomies

Standards:
HTML5 and responsive

SASS files included.

___________________________________________________________________

Maintainers:
Ryan Grønborg <ryan@gladfonden.dk>
___________________________________________________________________