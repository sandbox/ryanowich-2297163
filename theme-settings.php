<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *	 The form.
 * @param $form_state
 *	 The form state.
 */
function gladfagskole_form_system_theme_settings_alter(&$form, &$form_state) {

	$form['mtt_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('Theme Settings'),
		'#collapsible' => FALSE,
	'#collapsed' => FALSE,
	);

	$form['mtt_settings']['breadcrumb'] = array(
		'#type' => 'fieldset',
		'#title' => t('Breadcrumb'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	
	$form['mtt_settings']['breadcrumb']['breadcrumb_display'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show breadcrumb'),
		'#description'	 => t('Use the checkbox to enable or disable Breadcrumb.'),
		'#default_value' => theme_get_setting('breadcrumb_display','gladfagskole'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	
	$form['mtt_settings']['slideshow'] = array(
		'#type' => 'fieldset',
		'#title' => t('Front Page Slideshow'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	
	$form['mtt_settings']['slideshow']['slideshow_effect'] = array(
		'#type' => 'select',
		'#title' => t('Effects'),
		'#description'	 => t('From the drop-down menu, select the slideshow effect you prefer.'),
		'#default_value' => theme_get_setting('slideshow_effect','gladfagskole'),
		'#options' => array(
		'none' => t('none'),
		'fade' => t('fade'),
		'fadeout' => t('fadeout'),
		'scrollHorz' => t('scrollHorz'),
		),
	);
	
		$form['mtt_settings']['slideshow']['slideshow_slide_time'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide duration (sec)'),
		'#default_value' => theme_get_setting('slideshow_slide_time','gladfagskole'),
	);
	
	# Upload banner image
	# From https://drupal.org/comment/5274188#comment-5274188
	// Container fieldset
	$form['banner'] = array(
		'#type' => 'fieldset',
		'#title' => t('banner'),
	);
	// Default path for image
	$banner_path = theme_get_setting('banner_path');
	if (file_uri_scheme($banner_path) == 'public') {
		$banner_path = file_uri_target($banner_path);
	}
	// Helpful text showing the file name, disabled to avoid the user thinking it can be used for any purpose.
	$form['banner']['banner_path'] = array(
		'#type' => 'textfield',
		'#title' => 'Path to banner image',
		'#default_value' => $banner_path,
		'#disabled' => TRUE,
	);
	// Upload field
	$form['banner']['banner_upload'] = array(
		'#type' => 'file',
		'#title' => 'Upload banner image',
		'#description' => 'Upload a new image for the banner.',
	);
	// Attach custom submit handler to the form
	$form['#submit'][] = 'gladfagskole_settings_submit';
}
function gladfagskole_settings_submit($form, &$form_state) {
	$settings = array();
	// Check previous value
	$previousCheck = $form['banner']['banner_path']['#default_value'];
	if ($previousCheck == Null) {
		$prevousSet = false;
	}
	// Get the previous value
	$previous = 'public://' . $form['banner']['banner_path']['#default_value'];
	$file = file_save_upload('banner_upload');
	if ($file) {
		$parts = pathinfo($file->filename);
		$destination = 'public://' . $parts['basename'];
		$file->status = FILE_STATUS_PERMANENT;
		if(file_copy($file, $destination, FILE_EXISTS_REPLACE)) {
			$_POST['banner_path'] = $form_state['values']['banner_path'] = $destination;
			// If new file has a different name than the old one, delete the old
			if ( ($destination != $previous) && ($prevousSet) ) {
				drupal_unlink($previous);
			}
		}
	} else {
		// Avoid error when the form is submitted without specifying a new image
		$_POST['banner_path'] = $form_state['values']['banner_path'] = $previous;
	}
	
}


/**
* Implementation of hook_settings() for themes.
* From http://www.lullabot.com/blog/article/customizable-header-images-your-drupal-theme
*/
/*
function gladfagskole_settings($settings) {
	// This ensures that a 'files' directory exists if it hasn't
	// already been been created.
	file_check_directory(file_directory_path(), 
		FILE_CREATE_DIRECTORY, 'file_directory_path');

	// Check for a freshly uploaded header image, save it to the
	// filesystem, and grab its full path for later use.
	if ($file = file_save_upload('header_image',
			array('file_validate_is_image' => array()))) {
		$parts = pathinfo($file->filename);
		$filename = 'gladfagskole_header_image.'. $parts['extension'];
		if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
			$settings['header_image_path'] = $file->filepath;
		}
	}

	// Define the settings-related FormAPI elements.
	$form = array();
	$form['header_image'] = array(
		'#type' => 'file',
		'#title' => t('Header image'),
		'#maxlength' => 40,
	);
	$form['header_image_path'] = array(
		'#type' => 'value',
		'#value' => !empty($settings['header_image_path']) ?
			$settings['header_image_path'] : '',
	);
	if (!empty($settings['header_image_path'])) {
		$form['header_image_preview'] = array(
			'#type' => 'markup',
			'#value' => !empty($settings['header_image_path']) ? 
					theme('image', $settings['header_image_path']) : '',
		);
	}
	return $form;
}
*/
